<?php

namespace Drupal\drug;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the active_substance entity type.
 *
 * @see \Drupal\drug\Entity\ActiveSubstance
 */
class ActiveSubstanceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $active_substance, $operation, AccountInterface $account) {
    if ($operation == 'view') {
      return AccessResult::allowedIfHasPermission($account, 'access active substance');
    }
    else {
      return parent::checkAccess($active_substance, $operation, $account);
    }
  }

}

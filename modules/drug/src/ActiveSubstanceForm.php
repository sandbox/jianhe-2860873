<?php

namespace Drupal\drug;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the active_substance edit forms.
 */
class ActiveSubstanceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $active_substance = $this->entity;
    $insert = $active_substance->isNew();
    $active_substance->save();
    $active_substance_link = $active_substance->link($this->t('View'));
    $context = ['%title' => $active_substance->label(), 'link' => $active_substance_link];
    $t_args = ['%title' => $active_substance->link($active_substance->label())];

    if ($insert) {
      $this->logger('drug')->notice('ActiveSubstance: added %title.', $context);
      drupal_set_message($this->t('ActiveSubstance %title has been created.', $t_args));
    }
    else {
      $this->logger('drug')->notice('ActiveSubstance: updated %title.', $context);
      drupal_set_message($this->t('ActiveSubstance %title has been updated.', $t_args));
    }
  }

}

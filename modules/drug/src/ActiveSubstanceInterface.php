<?php

namespace Drupal\drug;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a active_substance entity.
 */
interface ActiveSubstanceInterface extends ContentEntityInterface {

}

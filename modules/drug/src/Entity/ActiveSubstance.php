<?php

namespace Drupal\drug\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\drug\ActiveSubstanceInterface;

/**
 * Defines the active_substance entity class.
 *
 * @ContentEntityType(
 *   id = "active_substance",
 *   label = @Translation("Active Substance"),
 *   handlers = {
 *     "access" = "Drupal\drug\ActiveSubstanceAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\drug\ActiveSubstanceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "active_substance",
 *   entity_keys = {
 *     "id" = "sid",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   admin_permission = "administer active substances",
 *   links = {
 *     "add-form" = "/admin/active_substance/add",
 *     "canonical" = "/admin/active_substance/{active_substance}",
 *     "edit-form" = "/admin/active_substance/{active_substance}/edit",
 *     "delete-form" = "/admin/active_substance/{active_substance}/delete",
 *     "collection" = "/admin/active_substance",
 *   }
 * )
 */
class ActiveSubstance extends ContentEntityBase implements ActiveSubstanceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the active_substance was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The timestamp that the active_substance was last changed.'));

    return $fields;
  }

}

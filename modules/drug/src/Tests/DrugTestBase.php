<?php

namespace Drupal\drug\Tests;

use Drupal\cbo_item\Tests\ItemTestBase;
use Drupal\drug\Entity\ActiveSubstance;

/**
 * Provides helper functions.
 */
abstract class DrugTestBase extends ItemTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['drug'];

  /**
   * A active substance.
   *
   * @var \Drupal\drug\ActiveSubstanceInterface
   */
  protected $activeSubstance;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->activeSubstance = $this->createActiveSubstance();
    $this->drug = $this->createDrug();
  }

  /**
   * Creates a drug based on default settings.
   */
  protected function createDrug(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'type' => 'drug',
    ];
    return $this->createItem($settings);
  }

  /**
   * Creates a active substance based on default settings.
   */
  protected function createActiveSubstance(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'title' => $this->randomMachineName(8),
      'number' => $this->randomMachineName(8),
    ];
    $entity = ActiveSubstance::create($settings);
    $entity->save();

    return $entity;
  }

}
